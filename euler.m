function [t, y]=euler(func, t, y0, h)
    t=t(1):h:t(2);  %przedzial czasowy
    y(1)=y0;    %warto�� pocz�tkowa
    %petla obliczajaca kolejne rozwiazania:
    for i = 1:length(t)-1
        y(i+1) = y(i) + 0.5*h*(feval(func,t(i),y(i)) + feval(func,t(i)+h,y(i)) + feval(func,t(i),y(i))*h);
    end
end