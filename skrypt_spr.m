
% @ - uchwyt funkcji
func=@(t,y)y^2-4;
y0=1;
t0=0;
tk=4;
h=[0.5, 0.25, 0.1, 0.05, 0.01, 0.001];

for i=1:length(h)
    [t1,y1]=euler(func,[t0 tk],y0,h(i));
    [t2,y2]=ode45(func,[t0 tk],y0);
    subplot(2,3,i);
    plot(t1,y1,t2,y2), grid;
    title(strcat('h=',num2str(h(i))));
end